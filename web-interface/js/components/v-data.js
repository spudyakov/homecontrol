Vue.component('v-data', {
	props: [
		'data_value',
		'data_key',
	],
	data() {
		return {
			titles: {
				humidity: 'percent',
				floor: 'degree',
				floor2: 'degree',
			},
		};
	},
	template: `
		<div class="control_row value">
			<div class="control_part left">
				<div class="control_title" v-html="$parent.translate[data_key][$parent.lang]"></div>
			</div>
			<div class="control_part right">
				<div class="control_btn bg-before-border">
					<div class="title" v-html="getDataValue"></div>
				</div>
			</div>
		</div>`,
	computed: {
		getDataValue() {
			if(this.titles[this.data_key] && this.$parent.translate[this.titles[this.data_key]]) {
				return `${Math.round(this.data_value)}${this.$parent.translate[this.titles[this.data_key]][this.$parent.lang]}`;
			} else
			if (this.$parent.translate[this.data_value]) {
				return this.$parent.translate[this.data_value][this.$parent.lang];
			} {
				return this.data_value;
			}
		}
	},
})