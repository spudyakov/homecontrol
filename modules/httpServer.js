const pug = require('pug');
const express = require('express');

const http_server = express();
http_server.set('view engine', 'pug');
http_server.use('/web-interface', express.static('./web-interface'));
http_server.locals.pretty = true;

module.exports = {
	init() {
		return new Promise((resolve, reject) => {
			let routes = ['/'].concat(Object.keys(db.getState()).map((key) => {
				return '/'+key;
			}));
			http_server
				.get(routes, (req, res) => {
					res.render('index', {
						state: JSON.stringify(db.getState()),
						port: serverParams.mqtt_http_port,
						translate: JSON.stringify(db.getTranslation())
					});
				})
				.get('/translate', (req, res) => {
					res.render('translate', {
						translate: JSON.stringify(db.getTranslation())
					});
				})
				.post('/translate', (req, res) => {
					try {
						let body = '';
						req.on('data', (data) => {
							body += data;
							if (body.length > 1e6) request.connection.destroy();
						});
						req.on('end', () => {
							db.updateTranslation(JSON.parse(body));
							db.saveTranslation(() => {
								res.json({status:'success'});
							})
						});
					} catch(err) {log(err)}
				})
				.listen(serverParams.http_port, serverParams.ip, () => {
					log(`HTTP server ready at ${serverParams.ip}:${serverParams.http_port}`);
					resolve();
				})
		})
	}
}