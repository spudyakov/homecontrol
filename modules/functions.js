module.exports = {
	getIp() {
		let ip;
		Object.values(require('os').networkInterfaces()).forEach((value) => {	
			value.forEach((iface) => {
				if(iface.family == 'IPv4' && iface.internal == false && /192\.168/.test(iface.address)) {
					ip = iface.address;
				}
			});
		});
		return ip;
	},
	log() {
		const timestamp = new Date(Date.now());
		let args = [String(timestamp.getHours()).padStart(2,'0')+':'+String(timestamp.getMinutes()).padStart(2,'0')+':'+String(timestamp.getSeconds()).padStart(2,'0')+'.'+String(timestamp.getMilliseconds()).padStart(3,'0')];
		for (let i = 0; i < arguments.length; i++) {
			args.push(arguments[i]);
		}
		console.log.apply(this, args);
	}
}